��    1      �  C   ,      8     9     P  
   d     o     t     {     �     �  )   �     �     �  $   �  ,     
   <     G      L     m      �  	   �  
   �     �     �  	   �     �  ,   �               '     ;     C     L     R     Z     `     f     �     �     �     �     �  	   �     �     �     �  	          0   "  0   S  s  �     �     	     2	  
   @	     K	  $   X	  $   }	     �	  4   �	     �	  
   
  7   
  @   N
  &   �
  
   �
  *   �
  &   �
  9        M     e          �     �     �  /   �     �       (        ?     S     i  
   p     {     �  #   �  #   �  -   �  
                  &     >     N  "   `     �     �  D   �  F   �     "   (      !                 1               -   +   '   0   /   	      .   *   ,                    )                                             
   %                            $                                           &   #         %s left for %s prayer %d hours %d minutes %d minutes 'Asr 'Ishaa Audio file to play azan Azan audio file Calculation method Choose the prayer time calculation method DST Dhuhr Egyptian General Authority of Survey Egyptian General Authority of Survey (Egypt) Enable DST Fajr Fixed Ishaa Interval (always 90) Hijri date correction Islamic Society of North America Jumaada I Jumaada II Last third of night Latitude Longitude Maghrib Manual correction for Hijri date calculation Midnight Muharram Muslim World League Rabi' I Rabi' II Rajab Ramadan Safar Salat Set the locations latitude Set the locations longitude Set the locations timezone Shaa'ban Shawwaal Shurooq Stop azan Thul Hijjah Thul Qi'dah Time now for %s prayer Time zone Umm Al-Qurra University of Islamic Sciences, Karachi (Hanafi) University of Islamic Sciences, Karachi (Shaf'i) Project-Id-Version: islamic-datetime 1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-06-30 18:17+0200
PO-Revision-Date: 2011-11-19 23:44+0200
Last-Translator: أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@sabily.org>>
Language-Team: Arabic <ar@li.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 باق %s حتى صلاة %s %d ساعة و %d دقيقة %d دقيقة العصر العشاء الملف الصوتي للأذان الملف الصوتي للأذان طريقة الحساب ضبط طريقة حساب مواقيت الصلاة التوقيت الصيفي الظهر الهيئة المصرية العامة للمساحة الهيئة المصرية العامة للمساحة (مصر) تنشيط التوقيت الصيفي الفجر فاصل عشاء ثابت (دائما 90) تصحيح التاريخ الهجري الجمعية الإسلامية لشمال أمريكا جمادى الأولى جمادى الثانية ثلث الليل الأخير خط العرض خط الطول المغرب تصحيح يدوي للتاريخ الهجري نصف الليل محرم رابطة العالم الإسلامي ربيع الأول ربيع الثاني رجب رمضان صفر الصلاة ضبط خط العرض لموقعك ضبط خط الطول لموقعك ضبط منطقة التوقيت لموقعك شعبان شوال الشروق إيقاف الأذان ذو الحجة ذو القعدة حان الآن وقت صلاة %s منطقة التوقيت تقويم أم القرى جامعة العلوم الإسلامية - كراتشي (حنفي) جامعة العلوم الإسلامية - كراتشي (شافعي) 