### Description

Hijri Date Time and Prayer is an Islamic extension for Gnome Shell. This is a fork of  [Sabily Date Time GNOME shell extension](https://extensions.gnome.org/extension/26/islamic-datetime-functions/).

### Features

- List 5 prayer times
- Displays: Sunrise, Sunset, Midnight
- Show remaining time for the upcoming prayer
- Show current date in Hijri calendar
- Display a notification when it's time for prayer
- Play call for prayer sound file
- Adjustement of Hijri date

### Installation

1. Download zip file : https://gitlab.com/hmasmoudi/islamic-datetime-gnome-shell-extension
2. Extract
3. make install

### Changelog

- 01 : initial upload with a reworked version with multiple fixes

### License

Licensed under the GNU General Public License, version 3

### Third-Party Assets & Components

- [Libitl](https://github.com/arabeyes-org/ITL)

